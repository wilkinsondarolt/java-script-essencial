(function() {
  'use strict';

  function winOnLoad() {
    var cpf = document.getElementById('cpf');
    var cep = document.getElementById('cep');
    var telefone = document.getElementById('telefone');

    var validations = {
      cpf: function(valor) {
        return valor
               .replace(/\D/g, '')
               .replace(/(\d{3})(\d)/, '$1.$2')
               .replace(/(\d{3})(\d)/, '$1.$2')
               .replace(/(\d{3})(\d{1,2})$/, '$1-$2')
               ;
      },
      cep: function(valor) {
        return valor
               .replace(/\D/g, '')
               .replace(/(\d{5})(\d)/, '$1-$2');
      },
      telefone: function(valor) {
        return valor
               .replace(/\D/g, '')
               .replace(/(\d{2})/, '($1) ')
               .replace(/(\9)/, '$1-')
               .replace(/(\d{4})(\d)/, '$1-$2');
      }
    }

    function onKeyUp() {
      this.value = validations[this.getAttribute('data-validation')](this.value);
    }
    addEvent(cpf, 'keyup', onKeyUp);
    addEvent(cep, 'keyup', onKeyUp);
    addEvent(telefone, 'keyup', onKeyUp);
  }
  addEvent(window, 'load', winOnLoad);
}());
