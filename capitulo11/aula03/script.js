(function() {
  'use strict';

  function winOnLoad() {
    var busca = document.getElementById('busca');
    var resultado = document.getElementById('resultado');
    var timer;
    function onBusca() {
        resultado.innerHTML = busca.value;
    }

    function onKeyUp() {
      clearTimeout(timer);
      timer = setTimeout(onBusca, 1000);
    }
    addEvent(busca, 'keyup', onKeyUp);
  }
  addEvent(window, 'load', winOnLoad);
}());
