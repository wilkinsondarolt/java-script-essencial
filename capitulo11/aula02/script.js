(function() {
  'use strict';

  function winOnLoad() {
    var formulario = document.getElementById('formulario');
    var mensagem = document.getElementById('mensagem');

    function formOnSubmit(evento) {
      var e = evento || window.event;
      e.preventDefault();

      var campos = formulario.elements;
      var serialize = '';
      var itens = campos.length;

      for (var i = 0; i < itens; i++) {
        var campo = campos[i];

        if (campo.name) {
          if (campo.value === '') {
            mensagem.innerHTML = 'Campo "' + campo.name + '" não foi informado.';
            return;
          }
          serialize += campo.name + '=' + campo.value + '&';
        }
      }
      serialize = serialize.substr(0, serialize.length-1);
      console.log(serialize);
    }
    addEvent(formulario, 'submit', formOnSubmit);
  }
  addEvent(window, 'load', winOnLoad);
}());
