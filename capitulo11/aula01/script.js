(function() {
  'use strict';

  function winOnLoad() {
    var formulario = document.getElementById('formulario');

    function formOnSubmit(evento) {
      var e = evento || window.event;
      e.preventDefault();

      var campos = formulario.elements;
      var serialize = '';
      var itens = campos.length;
      var dados = {};

      for (var i = 0; i < itens; i++) {
        var campo = campos[i];

        if (campo.name) {
          serialize += campo.name + '=' + campo.value + '&';
          dados[campo.name] = campo.value;
        }
      }
      serialize = serialize.substr(0, serialize.length-1);
      console.log(serialize);
      console.log(dados);
    }
    addEvent(formulario, 'submit', formOnSubmit);
  }
  addEvent(window, 'load', winOnLoad);
}());
