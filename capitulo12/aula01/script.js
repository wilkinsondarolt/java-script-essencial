(function() {
  'use strict';
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');

  // Desenha um quadrado
  ctx.fillStyle = 'black';
  ctx.fillRect(50, 100, 100, 100);
  ctx.strokeStyle = 'white';
  ctx.strokeRect(50, 100, 100, 100);

  // Desenha uma linha
  ctx.moveTo(0, 0);
  ctx.lineTo(500, 500);
  ctx.strokeStyle = 'orange';
  ctx.stroke();

  // Desenha um círculo
  ctx.beginPath();
  ctx.arc(350, 350, 80, 0, 2*Math.PI);
  ctx.stroke();
  ctx.fillStyle = 'black';
  ctx.fill();

  ctx.clearRect(0, 0, 500, 500);  
}());
