(function() {
  'use strict';
  // Variáveis de controle
  var canvas = document.getElementById('snake');
  var ctx = canvas.getContext('2d');
  var points = 0;
  var pixel = 16;
  var frames = 60;
  var loop = null;
  var flagTime = 0;

  const DIRECTION_LEFT = 'left';
  const DIRECTION_RIGHT = 'right';
  const DIRECTION_UP = 'up';
  const DIRECTION_DOWN = 'down';

  // Inicia o jogo
  var init = function() {
    screen.create();
    snake.create();
    food.create();
    text.create(5, screen.h-5, 'Pontos: ' + points, null, 'black');
    loop = setInterval(update, frames);
  };

  var update = function() {
    flagTime = frames;
    screen.update();
    snake.update();
    text.update();
    food.update();
    enemy.update();
  };

  var screen = {x: 0,
                y: 0,
                w: canvas.offsetWidth,
                h: canvas.offsetHeight,
                type: 'unique',
                color: 'orange',
                outline: 'black',
                checkCanvas: function() {
                  if ((this.w % 16 > 0)||(this.h % 16 > 0)) {
                    throw 'A largura e altura da tela devem ser divisíveis por ' + pixel + '\n' +
                          'Altura: ' + this.h + '\n' +
                          'Largura: ' + this.w;
                  }
                },
                create: function(color, outline) {
                  this.checkCanvas();
                  this.color = color || this.color;
                  this.outline = outline || this.outline;
                  paint(this);
                },
                update: function() {
                  paint(this);
                }
  }

  // Variável do jogador
  var snake = {w: 0,
               h: 0,
               type: 'multiple',
               color: 'white',
               outline: 'black',
               vector: [],
               size: 5,
               direction: DIRECTION_RIGHT,
               signalX: 0,
               signalY: 0,
               create : function() {
                 this.w = pixel;
                 this.h = pixel;
                 for (var i = 0; i < this.size; i++) {
                   this.vector.push({x: i*pixel, y: 0});
                 }
                 paint(this);
               },
               increment: function() {
                  var snakePart = {};
                  this.updateSignals();
                  snakePart.x = this.getHead().x + (this.signalX * pixel);
                  snakePart.y = this.getHead().y + (this.signalY * pixel);
                  this.vector.push(snakePart);
                  this.crossWall();
                  paint(this);
               },
               updateSignals: function() {
                 this.signalX = 0;
                 this.signalY = 0;
                 switch (this.direction) {
                   case DIRECTION_RIGHT:
                     this.signalX++;
                     break;
                   case DIRECTION_LEFT:
                    this.signalX--;
                    break;
                   case DIRECTION_UP:
                    this.signalY--;
                    break;
                   case DIRECTION_DOWN:
                    this.signalY++;
                    break;
                 }
               },
               update : function() {
                 if (checkColision(this.getHead(), food)){
                   food.destroy();
                   this.increment();
                   points++;
                   text.update();
                   enemy.create();
                 }
                 this.vector.shift();
                 this.updateSignals();
                 this.increment();
               },
               getPart : function(index) {
                 return this.vector[index];
               },
               getHead : function() {
                 return this.getPart(this.vector.length-1);
               },
               crossWall: function() {
                 switch (this.direction) {
                   case DIRECTION_RIGHT:
                    if (this.getHead().x === screen.w) {
                      this.getHead().x = 0;
                    }
                    break;
                   case DIRECTION_LEFT:
                    if (this.getHead().x === (-pixel)) {
                      this.getHead().x = (screen.w - pixel);
                    }
                    break;
                   case DIRECTION_UP:
                     if (this.getHead().y === (-pixel)) {
                       this.getHead().y = (screen.h - pixel);
                     }
                     break;
                   case DIRECTION_DOWN:
                     if (this.getHead().y === screen.h) {
                       this.getHead().y = 0;
                     }
                     break;
                 }
               }
  }

  // comida
  var food = {x: 0,
              y: 0,
              w: 0,
              h: 0,
              type: 'unique',
              color: 'red',
              outline: 'black',
              change: function() {
                this.x = Math.round(Math.random() * (screen.w - pixel));
                this.x = this.x-(this.x % pixel);
                this.y = Math.round(Math.random() * (screen.h - pixel));
                this.y = this.y-(this.y % pixel);

                var enemies = enemy.vector;

                for (var i = 0; i < enemies.length; i++) {
                  if (checkColision(this, enemies[i])) {
                    console.log('Criou em um inimigo.');
                    this.change();
                    break;
                  }
                }
              },
              create: function() {
                this.change();
                this.w = pixel;
                this.h = pixel;
                paint(this);
              },
              update: function(){
                paint(this);
              },
              destroy: function() {
                this.change();
                paint(this);
              }
  }
  // inimigo
  var enemy = {x: 0,
              y: 0,
              w: 0,
              h: 0,
              type: 'multiple',
              color: 'yellow',
              outline: 'black',
              vector: [],
              newEnemy: function() {
                var newEnemy = {};
                newEnemy.x = Math.round(Math.random() * (screen.w - pixel));
                newEnemy.x = newEnemy.x-(newEnemy.x % pixel);
                newEnemy.y = Math.round(Math.random() * (screen.h - pixel));
                newEnemy.y = newEnemy.y-(newEnemy.y % pixel);
                this.vector.push(newEnemy);
              },
              create: function() {
                this.w = pixel;
                this.h = pixel;
                this.newEnemy();
                paint(this);
              },
              update: function () {
                paint(this);
                this.checkColision();
              },
              checkColision: function() {
                var length = this.vector.length;
                for (var i = 0; i < this.vector.length; i++) {
                  if (checkColision(this.vector[i], snake.getHead())) {
                      gameOver();
                  }
                }
              }
  }

  // texto
  var text = {x: 0,
              y: 0,
              type: 'text',
              color: 'white',
              outline: 'black',
              font: '14px Trebuchet  MS',
              text: '',
              create: function(x, y, text, font, color) {
                this.x = x;
                this.y = y;
                this.text = text;
                this.font = font || this.font;
                this.color = color || this.color;
                paint(this);
              },
              update: function() {
                this.text = 'Pontos: ' + points;
                paint(this);
              }
  }

  var paint = function(obj) {
    var draw = function(x, y, w, h, color, outline) {
      ctx.fillStyle = color;
      ctx.fillRect(x, y, w, h);

      if (outline) {
        ctx.strokeStyle = outline;
        ctx.strokeRect(x, y, w, h);
      }
    }

    var text = function(x, y, text, font, color) {
      ctx.font = font;
      ctx.fillStyle = color;
      ctx.fillText(text, x, y);
    }


    try{
      switch (obj.type) {
        case 'unique':
          draw(obj.x, obj.y, obj.w, obj.h, obj.color, obj.outline);
          break;
        case 'multiple':
          for (var i = 0; i < obj.vector.length; i++) {
            draw(obj.vector[i].x, obj.vector[i].y, obj.w, obj.h, obj.color, obj.outline);
          }
          break;
        case 'text':
          text(obj.x, obj.y, obj.text, obj.font, obj.color);
          break;
        default:
          if ((obj.type === '')||(obj.type === undefined)) {
            throw 'Tipo do objeto não foi especificado.';
          } else {
            throw 'Tipo do objeto inválido.';
          }
      }
    } catch(e) {
      console.log(e);
    }
  }
  // quando pressionar as setas
  document.onkeydown = function(evt) {
    var e = evt||window.event;

    if (flagTime > 0) {
      flagTime = 0;

      switch (e.keyCode) {
        case 37:
        case 100:
        case 65:
          if (snake.direction !== DIRECTION_RIGHT) {
            snake.direction = DIRECTION_LEFT;
          }
          break;
        case 38:
        case 104:
        case 87:
          if (snake.direction !== DIRECTION_DOWN) {
            snake.direction = DIRECTION_UP;
          }
          break;
        case 39:
        case 102:
        case 68:
          if (snake.direction !== DIRECTION_LEFT) {
            snake.direction = DIRECTION_RIGHT;
          }
          break;
        case 40:
        case 98:
        case 83:
          if (snake.direction !== DIRECTION_UP) {
            snake.direction = DIRECTION_DOWN;
          }
          break;
        case 13:
          window.location.reload();
          break;
      }
    }
  }

  var checkColision = function(objectA, objectB) {
    if ((objectA) && (objectB)){
        return ((objectA.x === objectB.x) && (objectA.y === objectB.y)) ? true : false;
    }
    else {
      return false;
    }

  }

  var gameOver = function() {
    clearInterval(loop);
    var obj = {x: screen.w/2 - 150/2,
               y: screen.h/2 - 80/2,
               w: 150,
               h: 80,
               color: 'yellow',
               outline: 'black',
               type: 'unique'
    }
    paint(obj);
    text.create((screen.w/2)-55, screen.h/2-10, 'Fim de jogo', '22px Trebuchet MS', 'black');
    paint(text);
    text.create((screen.w/2)-45, screen.h/2+15, 'Pontos: ' + points, '22px Trebuchet MS', 'black');
    paint(text);
    document.getElementById('game-over').innerHTML = '<h4>Pressione [Enter] para reiniciar o jogo.</h4>'
  }

  init();
}());
