(function() {
  'use strict';

  function contaPar(inicio, fim) {
    var qt = 0;

    function VerificaPar(numero) {
      return numero % 2 == 0;
    }

    for (var i = inicio; i <= fim; i++) {
      if (VerificaPar(i)){
          console.log('Número ' + i.toString() + ' é par.');
          qt++;
      }
    }
    return qt;
  }

  console.log('Quantidade total de números pares: ' + contaPar(1, 300));
}());
