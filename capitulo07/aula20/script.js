(function() {
  'use strict';

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  var totalSorteados = 6
  var totalNumeros = 60;
  var numeros = [];

  do {
    var numero = getRandomInt(1, totalNumeros);

    if (numeros.indexOf(numero) == -1){
      numeros.push(numero);
    }
  } while (numeros.length < totalSorteados);
  numeros.sort(function(a, b){
                if(a === b) {
                    return 0;
                }
                if(typeof a === typeof b) {
                    return a < b ? -1 : 1;
                }
                return typeof a < typeof b ? -1 : 1;});
  console.log(numeros);
}());
