const Immutable = require('immutable');

function quickSort(numeros) {
  if (numeros.isEmpty()) {
    return numeros;
  }

  const pivo = numeros.first();
  const outros = numeros.rest();

  const menores = outros.filter(n => n < pivo);
  const maiores = outros.filter(n => n >= pivo);

  return quickSort(menores)
          .concat(pivo)
          .concat(quickSort(maiores));
}

const numeros = Immutable.List([100,2,10,12,9,200]);
const ordenados = quickSort(numeros);

console.log('Numeros', numeros);
console.log('Ordenados', ordenados);
