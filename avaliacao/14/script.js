(function() {
  'use strict';

  var entrada = [140, 30, 10, 15, 20, 1, 2, 120, 214, 350, 16, 25, 69, 24];

  function quickSort(ent, inicio, fim) {
    var numeros = [...ent];
    var i = inicio;
    var j = fim;
    var pivot = numeros[Math.round((inicio + fim) / 2)];
    var n = 0;

    while (i <= j){
      while(numeros[i] < pivot){
        i++;
      }
      while(numeros[j] > pivot){
        j--;
      }
      if (i <= j){
        n = numeros[i];
        numeros[i] = numeros[j];
        numeros[j] = n;
        i++;
        j--;
      }
    }
    if (inicio < j){
      numeros = quickSort(numeros, inicio, j);
    }
    if (i < fim){
      numeros = quickSort(numeros, i, fim);
    }
    return numeros;
  }
  var ordenado = quickSort(entrada, 0, entrada.length-1);
  console.log(ordenado);
}());
