(function() {
  'use strict';

  function calculaAreas(raios) {
    try {
      var areas = [];
      if (raios.length === 0) {
        throw 'Não foram informados raios de entrada.'
      }
      for (var i = 0; i < raios.length; i++) {
        var raio = raios[i];
        if (typeof raio !== "number") {
          throw 'O raio na posição ' + (i+1) + ' é de um tipo não suportado. \n' +
                'Esperado: ' + typeof i +' \n' +
                'Encontrado: ' + typeof raio;
        }
        areas[i] = (Math.PI * (raio * raio)).toFixed(5);
      }
      return areas;
    } catch (e) {
      console.log(e);
    }
  }

  var raios = [1, 2, 3, 4, 1, 5, 6];
  var areas = calculaAreas(undefined);
  console.log(areas);
}());
