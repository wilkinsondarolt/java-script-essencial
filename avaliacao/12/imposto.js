(function () {
  'user strict';

  function calculaImposto(valorproduto, percentual) {
    function calc(vlr, perc){
      return (vlr * (perc/100)).toFixed(2);
    }

    if (arguments.length < 2) {
      return function(perc) { return calc(valorproduto, perc); }
    } else  {
      return calc(valorproduto, percentual);
    }
  }

  var calcImposto = calculaImposto(500);
  console.log('Valor do ICMS -> ' + calculaImposto(500, 17));
  console.log('Valor do IPI -> ' + calculaImposto(500, 15));
  console.log('Valor do PIS -> ' + calculaImposto(500, 1.65));
  console.log('Valor do COFINS -> ' + calculaImposto(500, 7.6));

  console.log('Valor do ICMS -> ' + calcImposto(17));
  console.log('Valor do IPI -> ' + calcImposto(15));
  console.log('Valor do PIS -> ' + calcImposto(1.65));  
  console.log('Valor do COFINS -> ' + calcImposto(7.6));
}());
