(function() {
  'use strict';

  function soma(a, b) {
    function incremento(a){
      return ++a;
    }

    if (arguments.length < 1){
      return add;
    } else if (arguments.length < 2) {
      return function(c) { return incremento(a) + c}
    } else  {
      return a + b;
    }
    return a + b;
  }
  // Soma com dois parâmetros
  console.log(soma(1,2));
  // Versão curried
  var adds3 = soma(3);
  console.log(adds3(8));
}());
