(function() {
  'use strict';

  function soma(a, b) {
    return a+b;
  }

  function somaC(a) {
    return function(b) {return a + b;}
  }

  var incremento = somaC(1);

  // Soma com dois parâmetros
  somaC(1)(2);  
  console.log(soma(1,2));
  // Versão curried

  var adds3 = soma(3);
  console.log(adds3(8));
}());
