RegExp.prototype.findAll = function(str) {

    if(!this.global) {
        throw 'Modifier \'global(g)\' is required!';
    }

    var result, array = [];
    while(result = this.exec(str)) {
        array.push(result);
    }
    return array;
};