var frutas = ['Uva', 'Maçã', 'Banana', 'Pera', 'Maçã', 'Laranja', 'Tangerina'];

AngEliza = ['Angelica', 'Eliza'];
Peggy = ['and Peggy'];

schuylersisters = AngEliza.concat(Peggy);

console.log(schuylersisters.join());

console.log(frutas);
console.log(frutas.length);
console.log(frutas.indexOf('Maçã'));
console.log(frutas.lastIndexOf('Maçã'));


var numeros = [10, 20, 1, 5, 3, 6, 40, 25, 35, 16, 95];
console.log(numeros);
console.log(numeros.sort(
  function(a, b){
    if(a === b) {
        return 0;
    }
    if(typeof a === typeof b) {
        return a < b ? -1 : 1;
    }
    return typeof a < typeof b ? -1 : 1;

}));

var idades = [32, 33, 16, 40];
console.log(idades);

// Verifica se todos são maiores de idade
console.log( idades.every(function(idade) { return idade >= 18;}));

// Verifica se alguem é maior de idade
console.log( idades.some(function(idade) { return idade >= 18;}) );

// Filtra somente os adultos
var adultos = idades.filter(function(idade) { return idade >= 18;})
console.log(adultos);

// Retorna um array com o processamento das idades
var resultado = idades.map(function(idade) { return idade >= 18 ? 'Maior de idade' : 'Menor de idade'; })
console.log(resultado);

// Retorna a média de idade
var total = idades.reduceRight(function(vTot, vIdade) { return vTot + vIdade });

console.log(total);
console.log(idades.length);
console.log(Math.round(total / idades.length));
