// As primitive type
var str = 'Curso de javascript essencial';
console.log(typeof str);

// returns string length
console.log(str.length);
// returns character at position
console.log(str.charAt(2));
// returns character code at position
console.log(str.charCodeAt(2));
// returns the concatenation of two or more strings
console.log(str.concat(' muito bom!', 'Realmente.'));
// returns a string from the unicode characters given
console.log(String.fromCharCode(85, 86, 90));
// returns the first ocurrence position of the givecn string
console.log(str.indexOf('javascript'));
// returns the last ocurrence position of the givecn string
console.log(str.lastIndexOf('javascript'));
// checks if the string is sorted after of before the value
// returns -1 if string is sorted before the value
// returns 1 if string is sorted after
// returns 0 if string is sorted equally
console.log('ab'.localeCompare('cd')); // -1
console.log('cd'.localeCompare('ab')); // 1
console.log('cd'.localeCompare('cd')); // 0
// returns the ocurrences of the regular expression given
console.log(str.match(/javascript/g));
// Replacing a single occurrence
console.log(str.replace(' ', '+'));
// Replace all occurrences
console.log(str.replace(/ /g, '+'));
// Replace with function
console.log(str.replace(/a/gi, function(v){return v.toUpperCase()}));
// Replace with regular expression
console.log(str.replace(/([a-zA-Z]{5}) ([a-zA-Z]{2} ([a-zA-Z]{10}))/gi, '$3-$2-$1'));
// Search for a value in a string
console.log(str.search('javascript'));
console.log(str.search(/javascript/));
console.log(str.search(/javascript/i));
// Slices a string
console.log(str.slice(1, 5));
console.log(str.slice(5));
// Splits a string
console.log(str.split('de'));
console.log(str.split(''));
console.log(str.split(' '));
console.log(str.split(' '));

// Substr
// Copy 15 characters from 3
console.log(str.substr(3, 15));
// Copy from char 3 to 15
console.log(str.substring(3, 15));

// Upper and Lower case
console.log(str.toLowerCase());
console.log(str.toUpperCase());

// As Object
var str2 = new String('Curso de javascript essencial');
console.log(typeof str2.valueOf());

console.log('JavaScript'.valueOf());
console.log(typeof 'JavaScript'.valueOf());
