console.log(Number.MIN_VALUE);
console.log(Number.MAX_VALUE);
console.log(Number.NEGATIVE_INFINITY);
console.log(Number.POSITIVE_INFINITY);
console.log(Number.NaN);
console.log((342.65).toFixed(3));
console.log((345.9854).toPrecision(4));
console.log((345.9854).toExponential(6));
