(function() {
  'use strict';
  function winOnLoad() {
    var botao = document.getElementById('btn');
    var container = document.getElementById('container');

    addEvent(document.body, 'click', function functionName(e) {
      var evento = e||window.event;
      evento.stopPropagation ? evento.stopPropagation() : e.cancelBubble = true;
        console.log(e.type + ' - Body');
    });

    addEvent(container, 'click', function functionName(e) {
      var evento = e||window.event;
      evento.stopPropagation ? evento.stopPropagation() : e.cancelBubble = true;
        console.log(e.type + ' - Container');
    });

    addEvent(botao, 'click', function functionName(e) {
        var evento = e||window.event;
        evento.stopPropagation ? evento.stopPropagation() : e.cancelBubble = true;
        console.log(e.type + ' - Botão');
    });
  }
  addEvent(window, 'load', winOnLoad);
}());
