(function() {
  'use strict';

  var botao = document.getElementById('botao');
  var formulario = document.getElementById('form');

  function preventDef(e) {
    var evt = e || window.event;
    e.preventDefault ? e.preventDefault() : e.returnValue = false;
  }

  addEvent(botao, 'click', preventDef);
  addEvent(formulario, 'click', preventDef);
}());
