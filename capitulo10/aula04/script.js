(function() {
  'use strict';
  function onKeyPress(e) {
    console.log('{' + e.type + '} ' +
                'Tecla: ' + e.key );
  }

  function onClickBody(e) {
    console.log('{' + e.type + '} ' +
                'Mouse-x: ' + e.clientX + ' ' +
                'Mouse-y: ' + e.clientY);
  }
  addEvent(document.body, 'click', onClickBody);
  addEvent(document.body, 'mouseover', onClickBody);
  addEvent(document.body, 'keypress', onKeyPress);
}());
