(function() {
  'use strict';

  function funUm() {
    console.log('Função 1');
  }

  function funDois() {
    console.log('Função 2');
  }

  var botao = document.getElementById('botao');
  botao.addEventListener('click', funUm);
  botao.addEventListener('click', funDois);

  botao.removeEventListener('click', funDois);
}());
