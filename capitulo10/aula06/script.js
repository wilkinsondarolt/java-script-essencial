(function() {
  'use strict';

  function winOnLoad() {
    var botao = document.getElementById('botao');
    var formulario = document.getElementById('form');
    var campotexto = document.getElementById('texto');

    function preventDef(e) {
      var evt = e || window.event;
      e.preventDefault ? e.preventDefault() : e.returnValue = false;
      campotexto.blur();
    }
    addEvent(botao, 'click', preventDef);
    addEvent(formulario, 'click', preventDef);

    campotexto.focus();
  }

  addEvent(window, 'load', winOnLoad);
}());
