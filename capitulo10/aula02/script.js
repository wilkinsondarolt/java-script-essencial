(function() {
  'use strict';

  function criarElementoSimples(target, element, content) {
    setInterval(function() {
      var elem = document.createElement(element);
      elem.innerHTML = content;
      target.appendChild(elem);
    }, 3000);
  }

  function criarElemento(target, element, callback) {
    setInterval(function() {
      var elem = document.createElement(element);
      target.appendChild(elem);

      if (callback){
        callback(elem);
      }
    }, 3000);
  }

  criarElemento(document.body,
                'div',
                function (elemento) {
                  elemento.innerHTML = '<h1>Teste</h1>';
                  elemento.onclick = function() {
                                        console.log(this.innerHTML);
                                      }
                }
              );
}());
