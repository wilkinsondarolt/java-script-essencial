(function() {
  'use strict';

  var interval = setInterval(function() {
                              console.log(arguments[0]);
                            }, 2000, 'Olá mundo.');
  setTimeout(function () {
    console.log('Limpando interval.');
    clearInterval(interval);
  }, 10000);
}());
