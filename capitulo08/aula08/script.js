(function() {
  'use strict';
  var contador = 15;
  var texto = document.getElementById('contador');

  var interval = setInterval(function() {
                              texto.innerHTML = '<h1>' + contador-- + '<h1>';

                              if (contador === -1){
                                clearInterval(interval);
                              }
                            }, 1000);
}());
