(function() {
  console.log(navigator.appCodeName);
  console.log(navigator.appName);
  console.log(navigator.appVersion);
  console.log(navigator.cookieEnabled);
  console.log(navigator.language ? navigator.language : navigator.browserLanguage);
  console.log(navigator.onLine);
  console.log(navigator.product);
  console.log(navigator.userAgent);

  if (navigator.geoLocation){
    navigator.geoLocation.getCurrentPosition(function(position) {
      console.log(position.coords.latitude);
      console.log(position.coords.longitude);

      document.getElementById('map').innerHTML =  position.coords.latitude;
    });
  }
}());
