var Automovel = function(vTipo, vMarca, vModelo) {
    // private attributes
    var tipo = vTipo || 'Carro';
    var marca = vMarca || 'VW';
    var modelo = vModelo || 'Gol';

    // public methods
    this.ligar = function() {
      return modelo + ' ligado...';
    }

    this.andar = function () {
      return modelo + ' andando...';
    }

    this.getModelo = function () {
      return modelo;
    }
}

var vFusion = new Automovel('Carro', 'Ford', 'Fusion');
console.log(vFusion.ligar());
console.log(vFusion.andar());

var vFox = new Automovel('Carro', 'VW', 'Fox');
console.log(vFox.ligar());
console.log(vFox.andar());
