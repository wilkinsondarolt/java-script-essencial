var Cliente = function(vNome) {
  // private attributes
  var id;
  var nome = vNome || 'Fulano';
  var idade;

  this.getNome = function () {
    return nome;
  }

  this.getId = function() {
    return id;
  }

}

var will = new Cliente('Will');
console.log(will.getNome());
