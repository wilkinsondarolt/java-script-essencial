(function() {
  var pessoa = function(vNome, vIdade) {
    var nome = vNome || '';
    var idade = vIdade || '';

    this.setNome = function(vNome) {
      nome = vNome;

    }

    this.getNome = function() {
      return nome;
    }

    this.setIdade = function(vIdade) {
      idade = vIdade;
    }

    this.getIdade = function() {
      return idade;
    }
  }
  joao = new pessoa('João', 18);
  maria = new pessoa('Maria', 25);
  console.log(joao.getNome() + ' - ' + joao.getIdade());
  console.log(maria.getNome() + ' - ' + maria.getIdade());
})();
