// Declaração de variáveis (Usar camelCase)
var primeiro_nome = 'João';
var primeiro_Nome = 'Joaquim';
var primeironome = 'Rodrigo';
var primeiroNome = 'Marcelo';

function pegaNomeUsuario() {
  return primeiroNome;
}

// Declaração de constantes (UpperCase)
const CONSTANTE = 'consteste';

function teste() {
  var nome = 'João';
  var outro = 'outro';
}
