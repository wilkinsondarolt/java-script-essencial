(function() {
  'use strict';

  if (Storage !== undefined){
    console.log('O browser suporta o recurso de local storage.');

    if (!localStorage.getItem('username')){
      localStorage.setItem('username', 'wilkinsondarolt');
    }

    if (!localStorage.getItem('password')){
      localStorage.setItem('password', '12345');
    }
    console.log(localStorage.length + ' item(s) no localStorage');
    console.log(localStorage.getItem('username'));
    console.log(localStorage.getItem('password'));

    localStorage.removeItem('username');
    localStorage.removeItem('password');
  }
  else {
    console.log('O browser não suporta o recurso de local storage.');
  }
}());
