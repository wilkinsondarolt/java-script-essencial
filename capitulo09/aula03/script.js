(function() {
  'use strict';
  var elemen = document.getElementById('main');
  console.log(elemen.attributes);
  console.log(elemen.hasAttribute('id'));
  console.log(elemen.hasAttributes());
  console.log(elemen.id);
  console.log(elemen.classname);
  console.log(elemen.getAttribute('value'));
  elemen.setAttribute('value', 'new-text-attr');
  console.log(elemen.getAttributeNode('value').name);
  console.log(elemen.getAttributeNode('value').value);
}());
