(function() {
  'use strict';
  var elemen = document.getElementById('main');
  var newelemen = document.getElementById('mainnew');
  var endelemen = document.getElementById('mainend');

  elemen.style.height = '40px';
  elemen.style.width = '100px';
  elemen.style.backgroundColor = 'green';
  console.log(elemen.style);

  console.log(newelemen);

  newelemen.style.cssText = 'height: 40px; width: 100px; background-color: white';
  console.log(newelemen);

  endelemen.style.cssText = 'height: 40px; width: 100px; background-color: orange';
  console.log(endelemen);
}());
