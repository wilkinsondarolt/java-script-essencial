(function() {
  'use strict';
  var botao = document.createElement('button');
  var texto = document.createTextNode('Clique em mim');
  botao.appendChild(texto);
  botao.onclick = function () {
    console.log('Botão foi clicado');
  };
  var comentario = document.createComment('Cometário teste');
  var script = document.getElementsByTagName('script')[0];

  document.body.appendChild(botao);
  document.body.insertBefore(comentario, script);
}());
